//
//  AlbumCollectionVIewCell.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/4.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
