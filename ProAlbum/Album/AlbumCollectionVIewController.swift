//
//  AlbumCollectionVIewController.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/4.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import UIKit
import Lightbox

class AlbumCollectionVIewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var segueData:String?
    let pictureDao = PictureDao()
    var pictureArray = [UIImage]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.global().async {
            self.getCollectionData()
            DispatchQueue.main.async {
                self.cellLayoutSetting()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictureArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! AlbumCollectionViewCell
        cell.cellImage.image = pictureArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let images = [  LightboxImage(
            image: pictureArray[indexPath.row],
            text: ""
            )]
        let controller = LightboxController(images: images)
        
        // Set delegates.
        controller.pageDelegate = self as? LightboxControllerPageDelegate
        controller.dismissalDelegate = self as? LightboxControllerDismissalDelegate
        
        // Use dynamic background.
        controller.dynamicBackground = true
        controller.footerView.isHidden = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
    }
    
    func getCollectionData() {
        let result = pictureDao.findById(id: Int(segueData!)!)
        for data in result {
            let imageData = Data(base64Encoded: data.localAddress, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
            pictureArray.append(UIImage(data: imageData)!)
        }
    }
    
    func cellLayoutSetting() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: view.frame.width/4, height: view.frame.width/4)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
    }
}
