//
//  AlbumService.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import Foundation
import SQLite
import UIKit

class AlbumService {
    var database : Connection!
    let personTable = Table("Person")
    let pictureTable = Table("Picture")
    let personId = Expression<Int>("person_id")
    let name = Expression<String>("name")
    let id = Expression<Int>("id")
    let localAddress = Expression<String>("local_address")
    
    func findAllPerson() -> [Person]{
        self.database = DatabaseHelper.postRequest()
        var result = [Person]()
        do{
            for temp in try database.prepare(personTable) {
                let person = Person(id: "", name: "",image:"",height:"",weight:"",age:"")
                person.id = String(temp[personId])
                person.name = temp[name]
                result.append(person)
            }
            
        }catch{
            print(error)
        }
        return result
    }
    
    func findPersonAllPicture(pId:Int) -> [Picture]{
        self.database = DatabaseHelper.postRequest()
        var result = [Picture]()
        do{
            let selectedData = self.pictureTable.filter(self.personId == pId)
            let selectedDatasb = try self.database.prepare(selectedData)
            for temp in selectedDatasb {
                let picture = Picture(id: "",localAddress:"",personId:"")
                picture.id = String(temp[id])
                picture.localAddress = temp[localAddress]
                picture.personId = String(temp[personId])
                result.append(picture)
            }
            
        }catch{
            print(error)
        }
        return result
    }
}
