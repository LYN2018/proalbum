//
//  AlbumController.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import UIKit
import ImagePicker
import Lightbox
import Photos

class AlbumController: UIViewController, UITableViewDelegate, UITableViewDataSource, ImagePickerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    var indexArray = [String]()
    var personArray = [String]()
    var localIdArray = [String]()
    var personId:Int?
    let albumService = AlbumService()
    let personDao = PersonDao()
    let pictureDao = PictureDao()
    let imagePickerController = ImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshData()
        imagePickerController.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        animationTable()
        let notificationName = Notification.Name(rawValue: "DownloadImageNotification")
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(AlbumController.refreshData),
                                               name: notificationName, object: nil)
    }

    @IBAction func AddPerson(_ sender: UIBarButtonItem) {
        showInputDialog()
    }

    func showInputDialog() {
        //Creating UIAlertController and
        //Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Search", message: "Enter the name of your model", preferredStyle: .alert)

        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in

            //getting the input values from user
            let name = alertController.textFields?[0].text
            var height = alertController.textFields?[1].text
            var weight = alertController.textFields?[2].text
            var age = alertController.textFields?[3].text
            if height?.isEmpty == true {
                height = ""
            }
            if weight?.isEmpty == true {
                weight = ""
            }
            if age?.isEmpty == true {
                age = ""
            }
            if (name ?? "").isEmpty {
                self.createAlert(title: "Fail", message: "Please input the name!")
            }else{
                self.personDao.insert(name: name!,height: height!,weight: weight!, age: age!)
                self.createAlert(title: "Succeed", message: "Succeed add the person!")
                self.refreshData()
            }
        }

        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }

        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "Name(required):Lily"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Height(optional):180.0"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Weight(optional):70.0"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Age(optional):24"
        }

        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)

        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func animationTable(){
        tableView.reloadData()
        
        //获取可见的cell
        let cells = tableView.visibleCells
        
        //获取tableview高度
        let height : CGFloat = tableView.bounds.height
        
        //遍历，并设置每个cell的位置
        for i in cells {
            let cell : UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: height)
        }
        
        //设置动画
        
        var index = 0.0
        
        //遍历并设置动画
        for item in cells {
            let cell : UITableViewCell = item as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * index, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)
            index += 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailTableViewCell
        cell.cellLabel.text = personArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result = pictureDao.findById(id: Int(indexArray[indexPath.row])!)
        if result.isEmpty {
            self.personId = Int(indexArray[indexPath.row])!
            present(imagePickerController, animated: true, completion: nil)
        } else{
            performSegue(withIdentifier: "connectSegue", sender: indexArray[indexPath.row])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let guest = segue.destination as! AlbumCollectionVIewController
        guest.segueData = sender as? String
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let index = self.indexArray[indexPath.row]
        let name = self.personArray[indexPath.row]
        let alert = UIAlertController(title:"Confirm", message:"Are you sure to delete \(name)" ,preferredStyle:UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title:"Yes", style:UIAlertActionStyle.default, handler:{(action) in
            self.indexArray.remove(at: indexPath.row)
            self.personDao.deleteById(id: Int(index)!)
            self.refreshData()
            self.createAlert(title: "Succeed", message: "Deleted the data!")}))
        alert.addAction(UIAlertAction(title:"No", style:UIAlertActionStyle.default, handler:{(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        if editingStyle == .delete {
            self.present(alert, animated: true, completion: nil)
            tableView.reloadData()
        }
    }
    
    func createAlert (title:String, message:String){

        let alert = UIAlertController(title:title, message:message ,preferredStyle:UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title:"OK", style:UIAlertActionStyle.default, handler:{(action) in
            alert.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
        let lightboxImages = images.map {
            return LightboxImage(image: $0)
        }
        
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        imagePicker.present(lightbox, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        DispatchQueue.global().async {
            self.saveImage(images: images)
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func saveImage(images: [UIImage]){
        for data in images {
            let image = data
            let imageData = UIImagePNGRepresentation(image)
            let result = imageData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
            self.pictureDao.insert(image64: result!,personId: self.personId!)
        }
    }
    
    @objc func refreshData() {
        self.indexArray.removeAll()
        self.personArray.removeAll()
        let result = albumService.findAllPerson()
        for data in result {
            self.indexArray.append(data.id)
            self.personArray.append(data.name)
        }
        self.tableView.reloadData()
    }
}
