//
//  PictureDao.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import Foundation
import SQLite

class PictureDao {
    var database : Connection!
    let pictureTable = Table("Picture")
    let id = Expression<Int>("id")
    let localAddress = Expression<String>("local_address")
    let personId = Expression<Int>("person_id")
    
    func findById(id :Int) -> [Picture] {
        self.database = DatabaseHelper.postRequest()
        var result = [Picture]()
        do {
            let query = pictureTable.filter(self.personId == id)
            let resultList = try self.database.prepare(query)
            for data in resultList {
                let picture = Picture(id: "", localAddress: "", personId: "")
                picture.id = String(data[self.id])
                picture.localAddress = data[self.localAddress]
                picture.personId = String(data[self.personId])
                result.append(picture)
            }
        }catch{
            print(error)
        }
        return result
    }
    
    func insert(image64 :String,personId :Int) {
        self.database = DatabaseHelper.postRequest()
        do {
            let insert = pictureTable.insert(self.localAddress <- image64,self.personId <- personId)
            _ = try database.run(insert)
        }catch{
            print(error)
        }
    }
}
