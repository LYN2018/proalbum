//
//  PersonDao.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import Foundation
import SQLite

class PersonDao {
    var database : Connection!
    let personTable = Table("Person")
    let personId = Expression<Int>("person_id")
    let name = Expression<String>("name")
    let height = Expression<String>("height")
    let weight = Expression<String>("weight")
    let age = Expression<String>("age")

    func deleteById(id :Int) {
        self.database = DatabaseHelper.postRequest()
        do {
            let result = personTable.filter(self.personId == id)
            try database.run(result.delete())
        }catch{
            print(error)
        }
    }
    
    func insert(name :String,height :String,weight :String,age :String) {
        self.database = DatabaseHelper.postRequest()
        do {
            let insert = personTable.insert(self.name <- name,self.height <- height,self.weight <- weight,self.age <- age)
            _ = try database.run(insert)
        }catch{
            print(error)
        }
    }
}
