//
//  Person.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import Foundation

class Person: CustomStringConvertible {
    var id: String
    var name: String
    var image: String
    var height: String
    var weight: String
    var age: String
    
    init(id: String, name: String, image: String, height: String, weight: String, age: String) {
        self.id = id
        self.name = name
        self.image = image
        self.height = height
        self.weight = weight
        self.age = age
    }
    
    var description: String {
        
        return "id：\(id) ，name：\(name)，image：\(image)，height：\(height)，weight：\(weight)，age：\(age)"
        
    }
}
