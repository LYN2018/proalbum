//
//  Picture.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import Foundation

class Picture: CustomStringConvertible {
    var id: String
    var localAddress: String
    var personId: String
    
    init(id: String, localAddress: String, personId: String) {
        self.id = id
        self.localAddress = localAddress
        self.personId = personId
    }
    
    var description: String {
        
        return "id：\(id) ，localAddress：\(localAddress) ，personId：\(personId)"
        
    }
}
