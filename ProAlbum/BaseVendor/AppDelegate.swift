//
//  AppDelegate.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/3.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import UIKit
import SQLite

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var database : Connection!
    let personTable = Table("Person")
    let pictureTable = Table("Picture")
    let albumTable = Table("Album")
    let tokenTable = Table("Token")
    let personId = Expression<Int>("person_id")
    let name = Expression<String>("name")
    let image = Expression<String>("image")
    let height = Expression<Double>("height")
    let weight = Expression<Double>("weight")
    let age = Expression<String>("age")
    let id = Expression<Int>("id")
    let localAddress = Expression<String>("local_address")
    let number = Expression<Int>("number")
    let date = Expression<String>("date")
    let token = Expression<String>("token")


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // 得到当前应用的版本号
        let infoDictionary = Bundle.main.infoDictionary
        let currentAppVersion = infoDictionary!["CFBundleShortVersionString"] as! String
        
        // 取出之前保存的版本号
        let userDefaults = UserDefaults.standard
        let appVersion = userDefaults.string(forKey: "appVersion")
        
        // 第一次启动创建数据库
        if appVersion == nil {
            self.database = DatabaseHelper.postRequest()
            
            let createPersonTable = self.personTable.create{(table) in
                table.column(self.personId,primaryKey:true)
                table.column(self.name)
                table.column(self.image,defaultValue:"")
                table.column(self.height,defaultValue:0.0)
                table.column(self.weight,defaultValue:0.0)
                table.column(self.age,defaultValue:"")
            }
            
            let createPictureTable = self.pictureTable.create{(table) in
                table.column(self.id,primaryKey:true)
                table.column(self.localAddress)
                table.column(self.personId)
            }
            
            let createAlbumTable = self.albumTable.create{(table) in
                table.column(self.id,primaryKey:true)
                table.column(self.number)
            }
            
            let tokenTable = self.tokenTable.create{(table) in
                table.column(self.id,primaryKey:true)
                table.column(self.token)
                table.column(self.date)
            }
            
            do{
                self.database = DatabaseHelper.postRequest()
                try self.database.run(createPersonTable)
                print("Created Person Table")
            }catch{
                print(error)
            }
            
            do{
                self.database = DatabaseHelper.postRequest()
                try self.database.run(createPictureTable)
                print("Created Picture Table")
            }catch{
                print(error)
            }
            
            do{
                self.database = DatabaseHelper.postRequest()
                try self.database.run(createAlbumTable)
                print("Created Album Table")
            }catch{
                print(error)
            }
            
            do{
                self.database = DatabaseHelper.postRequest()
                try self.database.run(tokenTable)
                print("Created Token Table")
            }catch{
                print(error)
            }
            
        }
        
        // 如果 appVersion 为 nil 说明是第一次启动；如果 appVersion 不等于 currentAppVersion 说明是更新了
        if appVersion == nil || appVersion != currentAppVersion {
            
            // 保存最新的版本号
            userDefaults.setValue(currentAppVersion, forKey: "appVersion")
            
            
        }
        
        let mainStoryBoard = UIStoryboard(name:"Main",bundle:nil)
        let tabBarViewController = mainStoryBoard.instantiateViewController(withIdentifier: "TabController") as! TabController
        self.window?.rootViewController = tabBarViewController
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

