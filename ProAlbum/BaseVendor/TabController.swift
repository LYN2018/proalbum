//
//  TabController.swift
//  ProAlbum
//
//  Created by 李亚男 on 2018/6/4.
//  Copyright © 2018年 李亚男. All rights reserved.
//

import UIKit

class TabController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 获取两个Tab对应的 Storyboard
        let story_album = UIStoryboard(name:"Album",bundle:nil)
        let story_calculate = UIStoryboard(name:"Calculate",bundle:nil)
        
        // 获取对应的 ViewController
        let album_viewcontroller = story_album.instantiateViewController(withIdentifier: "albumNavi") as! UINavigationController
        let calculate_viewcontroller = story_calculate.instantiateViewController(withIdentifier: "calculateNavi")
        
        // 设置 tabBar 内容
        album_viewcontroller.tabBarItem = UITabBarItem(title:"Album",image:nil,tag:0)
        calculate_viewcontroller.tabBarItem = UITabBarItem(title:"Calculate",image:nil,tag:1)
        album_viewcontroller.tabBarItem.image=UIImage(named: "")?.withRenderingMode(.alwaysOriginal) // deselect image
        album_viewcontroller.tabBarItem.selectedImage = UIImage(named: "")?.withRenderingMode(.alwaysOriginal) // select image
        calculate_viewcontroller.tabBarItem.image=UIImage(named: "")?.withRenderingMode(.alwaysOriginal) // deselect image
        calculate_viewcontroller.tabBarItem.selectedImage = UIImage(named: "")?.withRenderingMode(.alwaysOriginal) // select image
        
        self.viewControllers = [album_viewcontroller,calculate_viewcontroller]
    }
}
